﻿#ifndef G_FHS
#define G_FHS

#define FHS_ETC "../etc/"
#define FHS_LIB "../lib/"
#define FHS_SHARE "../share/"

#define CFG_FILE "../etc/config.json"
#define ENT_NAME "GalMain.qml"

#define APP_VER "0.2.0"

#endif // G_FHS


﻿#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QUrl>
#include <QIcon>
#include <QResource>
#include <QQmlContext>
#include "config.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    _Config config;

    if(!config.load())
        return -1;

    app.setWindowIcon(QIcon(FHS_SHARE + config.ico));

    if(config.resFlag)
    {
        QResource::registerResource( FHS_LIB + config.std);
        QResource::registerResource( FHS_LIB + config.bg);
        QResource::registerResource( FHS_LIB + config.cg);
    }

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("cfg_title",config.title);
    engine.rootContext()->setContextProperty("cfg_widget",config.width);
    engine.rootContext()->setContextProperty("cfg_height",config.height);
    engine.load(QUrl(config.entry));

    return app.exec();
}


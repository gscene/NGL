﻿#ifndef CONFIG_H
#define CONFIG_H

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QString>
#include <QByteArray>
#include <QFile>

#include "g_fhs.h"

struct _Config
{
    bool load(const QString &file= CFG_FILE);
    void read(const QJsonObject &json);

    QString title;
    QString ico;
    int width;
    int height;
    QString entry;

    bool resFlag;

    QString std;
    QString cg;
    QString bg;
};

#endif // CONFIG_H

﻿#include "config.h"

void _Config::read(const QJsonObject &json)
{
    title=json["title"].toString();
    ico=json["ico"].toString();
    width=json["width"].toInt();
    height=json["height"].toInt();
    entry=json["entry"].toString();

    if(entry.isEmpty())
        entry=FHS_LIB + QString(ENT_NAME);
    else
        entry=FHS_LIB + entry;

    QJsonObject assets=json["assets"].toObject();
    if(assets.isEmpty())
    {
        resFlag=false;
        return;
    }
    else
    {
        std=assets["std"].toString();
        cg=assets["cg"].toString();
        bg=assets["bg"].toString();
        resFlag=true;
    }
}

bool _Config::load(const QString &file)
{
    QFile config(file);
    if(config.open(QFile::ReadOnly))
    {
        QByteArray data=config.readAll();
        QJsonDocument jsonDoc=QJsonDocument::fromJson(data);
        read(jsonDoc.object());
        return true;
    }
    else
        return false;
}

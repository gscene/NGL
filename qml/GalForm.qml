﻿import QtQuick 2.5

Rectangle {
    id:root

    Image {
        id: bg
        anchors.fill: parent
        z:1
        source: "qrc:/bg/bg005.jpg"
    }

    Image {
        id: fg
        anchors.bottom: parent.bottom
        x:parent.width / 2
        z:2
        source: "qrc:/std/std_02.png"
    }

    Text {
        id: label
        z:3
        font.pixelSize: 32
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 50
        anchors.left: parent.left
        anchors.leftMargin: 200
        text: qsTr("你好，世界")

        MouseArea{
            anchors.fill: parent
            onClicked: {
                fg.x = root.width /2 - 250
                label.text="欢迎光临"
            }
        }
    }
}


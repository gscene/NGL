﻿import QtQuick 2.5
import QtQuick.Controls 1.4

ApplicationWindow {
    visible: true
    minimumWidth: cfg_widget
    minimumHeight: cfg_height
    title: cfg_title
    menuBar: GalMenu{}

    GalForm {
        anchors.fill: parent
    }
}

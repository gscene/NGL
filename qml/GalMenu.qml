import QtQuick 2.5
import QtQuick.Controls 1.4

MenuBar {
    Menu {
        title: qsTr("文件")
        MenuItem {
            text: qsTr("打开(&O)")
            onTriggered: console.log("Open action triggered");
        }
        MenuItem {
            text: qsTr("退出(&Q)")
            onTriggered: Qt.quit();
        }
    }

    Menu {
        title: "画面"
        MenuItem {
            text: "全屏(&F)"
            onTriggered: console.log("Open action triggered");
        }
    }

    Menu {
        title: "帮助"
        MenuItem {
            text: "帮助(&H)"
            onTriggered: console.log("Open action triggered");
        }
        MenuItem {
            text: "关于(&B)"
        }
    }
}

TEMPLATE = app

QT += qml quick

SOURCES += main.cpp \
    config.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    config.h \
    g_fhs.h

DISTFILES += \
    qml/GalForm.qml \
    qml/GalMain.qml \
    qml/GalMenu.qml

